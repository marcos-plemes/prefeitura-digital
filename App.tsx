import { StatusBar } from 'react-native';
import { FontAwesome } from "@expo/vector-icons";
import React, { useEffect } from "react";
import { useFonts } from 'expo-font';
import { Nunito_200ExtraLight, Nunito_200ExtraLight_Italic, Nunito_300Light, Nunito_300Light_Italic, Nunito_400Regular, Nunito_400Regular_Italic, Nunito_500Medium, Nunito_500Medium_Italic, Nunito_600SemiBold, Nunito_600SemiBold_Italic, Nunito_700Bold, Nunito_700Bold_Italic, Nunito_800ExtraBold, Nunito_800ExtraBold_Italic, Nunito_900Black, Nunito_900Black_Italic } from "@expo-google-fonts/nunito";
import LoginScreen from "./src/screens/loginscreen/LoginScreen";

export default function App() {

    useEffect(() => {
        StatusBar.setBarStyle('light-content');

    }, []);

    const [fontsLoaded, error] = useFonts({
        IcoMoon: require('./assets/fonts/mega-icon/fonts/mega-icon.ttf'),
        Nunito_200ExtraLight,
        Nunito_300Light,
        Nunito_400Regular,
        Nunito_500Medium,
        Nunito_600SemiBold,
        Nunito_700Bold,
        Nunito_800ExtraBold,
        Nunito_900Black,
        Nunito_200ExtraLight_Italic,
        Nunito_300Light_Italic,
        Nunito_400Regular_Italic,
        Nunito_500Medium_Italic,
        Nunito_600SemiBold_Italic,
        Nunito_700Bold_Italic,
        Nunito_800ExtraBold_Italic,
        Nunito_900Black_Italic,
        ...FontAwesome.font
    });

    useEffect(() => {
        if (error) {
            throw error;
        }
    }, [error]);

    if (!fontsLoaded) {
        return null;
    }

    return (
        <LoginScreen/>
    );

}


