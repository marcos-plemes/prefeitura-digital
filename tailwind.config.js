/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './App.{js,jsx,ts,tsx}',
        './src/(**)/*.{js,jsx,ts,tsx}'
    ],
    theme: {
        extend: {
            fontSize: {
                '3xx': 30
            },
            fontFamily: {
                'nunito-regular': "Nunito_400Regular",
                'nunito-semi-bold': "Nunito_600SemiBold",
                'nunito-bold': "Nunito_700Bold"
            },
            colors: {
                'login-header': {
                    50: '#29f4de',
                    150: '#0b9b8d'
                },
                'aba-button': {
                    1: '#aab0b7'
                },
                neutral: {
                    110: '#f1f1f1',
                    210: '#dbdbdb',
                    290: '#d7d7d7'
                },
                field: {
                    50: '#3a424c'
                }

            }
        }

    },
    plugins: []
};

