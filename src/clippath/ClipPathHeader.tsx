import { Dimensions, View } from "react-native";
import Svg, { LinearGradient, Path, Stop } from "react-native-svg";

const { width, height } = Dimensions.get('window');
export default function ClipPathHeader() {

    return (
        <>
            <View className="absolute top-0 w-full h-10 bg-login-header-50"/>
            <View className="absolute top-5 left-0">
                <Svg height={height * 0.399} width={width * 0.93} style={{ backgroundColor: '#00000000' }}>
                    <Path
                        d={`M0 ${height * 0.399} L${width * 0.93} ${height * 0.251} L${width} 0 L0 0 Z`}
                        fill="#f8e71c">
                    </Path>
                </Svg>
            </View>

            <View className="absolute top-5 left-0">
                <Svg height={height * 0.500} width={width} style={{ backgroundColor: '#00000000' }}>
                    <LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
                        <Stop offset="0%" stopColor="#29f4de"/>
                        <Stop offset="100%" stopColor="#0eafae"/>
                    </LinearGradient>
                    <Path
                        d={`M0 ${height * 0.374} L${width} ${height * 0.214} L${width} 0 L0 0 Z`}
                        fill="url(#grad)">
                    </Path>
                </Svg>
            </View>
        </>
    );
}
