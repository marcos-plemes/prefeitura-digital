enum TipoDeInscricaoEnum {
    IMOBILIARIA = 'imobiliaria',
    MUNICIPAL = 'municipal',
    NAO_TENHO = 'naoTenho',
}

export function lookupTipoDeIncricaoEnum(valor: string): TipoDeInscricaoEnum | undefined {
    return TipoDeInscricaoEnum[valor as keyof typeof TipoDeInscricaoEnum];
}

export default TipoDeInscricaoEnum;