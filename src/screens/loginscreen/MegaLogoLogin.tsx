import { Image, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import MegaIcon from "../../components/MegaIcon";
import Usuario from "../../entidades/Usuario";
import LoginService from "./LoginService";

interface MegaLogoLoginProps {

    readonly usuario?: Usuario;

    readonly setUsuario: (usuario: Usuario) => void;
}

export default function MegaLogoLogin({ usuario, setUsuario }: MegaLogoLoginProps) {
    const [logo, setLogo] = useState<string>();

    useEffect(() => {
        if (usuario?.municipio?.linkDeAcesso) {
            const gerarLogo = async() => {
                const codigoDoOrgao = await LoginService.buscarCodigoDoOrgao(usuario?.municipio?.linkDeAcesso as string);
                setLogo(await LoginService.buscarLogo(usuario?.municipio?.linkDeAcesso as string, codigoDoOrgao));
                // setUsuario({
                //     ...usuario,
                //     codigoDoOrgao: codigoDoOrgao
                // });

            };
            gerarLogo();
        }

    }, [usuario?.municipio?.linkDeAcesso]);

    return (
        <>
            <View className="w-60">
                <Text className="text-center text-white font-nunito-bold text-3xx"
                      adjustsFontSizeToFit={true}
                      numberOfLines={1}>
                    PREFEITURA DIGITAL
                </Text>
            </View>
            <View className="w-60 mb-6">
                <Text className="text-center text-login-header-150 font-nunito-semi-bold text-3xx"
                      adjustsFontSizeToFit={true}
                      numberOfLines={1}>
                    Serviços Online para toda a população.
                </Text>
            </View>
            <View className="w-60 h-48 p-2 rounded-lg bg-white mb-7"
                  style={{
                      elevation: 7,
                      shadowColor: '#000',
                      shadowOpacity: 1,
                      shadowRadius: 0,
                      shadowOffset: {
                          width: 0,
                          height: 7
                      }
                  }}>
                {usuario?.municipio?.linkDeAcesso && <Image source={{ uri: `data:image/png;base64,${logo}` }} style={{
                    width: '100%',
                    height: '100%',
                    resizeMode: 'contain'
                }}/>}

                {!usuario?.municipio?.linkDeAcesso && <View className="w-56 h-44 rounded-lg bg-neutral-100 justify-center	items-center">
                    <MegaIcon name="mega-pref" size={70} color="#d7d7d7"/>
                    <Text className="text-base text-center text-neutral-290 mt-2.5 px-7 font-nunito-regular">
                        Preencha os seus dados de registro
                    </Text>
                </View>}
            </View>
        </>
    );
}