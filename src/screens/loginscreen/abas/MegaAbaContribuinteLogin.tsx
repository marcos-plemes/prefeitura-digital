import { Text, View } from "react-native";
import React, { useState } from "react";
import { RadioButton } from "react-native-paper";
import { TextInputMask, TextInputMaskTypeProp } from "react-native-masked-text";
import { cnpj, cpf } from "cpf-cnpj-validator";
import { MegaLoginScreenProps } from "../LoginScreen";

export default function MegaAbaContribuinteLogin(props: MegaLoginScreenProps) {
    const { usuario, setUsuario } = props;
    const [tipoDeDocumentacao, setTipoDeDocumentacao] = useState('cpf');

    const getTipoDeDocumentacao = function(): TextInputMaskTypeProp {
        return tipoDeDocumentacao as "cpf" | "cnpj";
    };

    const limparCpfCnpj = function(tipoDeDocumentacao: string): void {
        setUsuario({
            ...usuario,
            cpfCnpj: ''
        });
        setTipoDeDocumentacao(tipoDeDocumentacao);
    };

    const setCpfCnpj = function(cpfCnpj: string): void {
        setUsuario({
            ...usuario,
            cpfCnpj: cpfCnpj
        });
    };

    const validarCpf = function(): boolean {
        return usuario?.cpfCnpj?.length === 14 && !cpf.isValid(usuario?.cpfCnpj);
    };

    const validarCnpj = function(): boolean {
        return usuario?.cpfCnpj?.length === 18 && !cnpj.isValid(usuario?.cpfCnpj);
    };
    const validarCampoCpfCnpj = function(): boolean {
        return tipoDeDocumentacao === 'cpf' ? validarCpf() : validarCnpj();
    };

    return (
        <View className="px-8 w-full" {...props}>
            <Text className="text-sm text-field-50 font-nunito-semi-bold">
                Tipo de Documentação
            </Text>
            <View>
                <RadioButton.Group onValueChange={limparCpfCnpj} value={tipoDeDocumentacao}>
                    <View className="flex-row">
                        <RadioButton.Item style={{ paddingLeft: 0 }} labelStyle={{ fontFamily: 'Nunito_400Regular', fontSize: 14 }} label="CPF" value="cpf" position="leading"/>
                        <RadioButton.Item style={{ paddingLeft: 0 }} labelStyle={{ fontFamily: 'Nunito_400Regular', fontSize: 14 }} label="CNPJ" value="cnpj" position="leading"/>
                    </View>
                </RadioButton.Group>
            </View>
            <TextInputMask className="border border-[#dbdbdb] font-nunito-regular px-3.5 h-10 text-base rounded-[3px]"
                           style={{
                               borderColor: validarCampoCpfCnpj() ? 'red' : '#dbdbdb',
                               marginBottom: validarCampoCpfCnpj() ? 0 : 13
                           }}
                           type={getTipoDeDocumentacao()}
                           onChangeText={setCpfCnpj}
                           value={usuario?.cpfCnpj}
                           maxLength={tipoDeDocumentacao === 'cpf' ? 14 : 18}
                           placeholder="Escreva somente números"/>
            {validarCampoCpfCnpj() && <Text style={{
                fontSize: 12,
                marginLeft: 5,
                color: 'red',
                height: 13
            }}>
                {tipoDeDocumentacao.toUpperCase()} inválido!
            </Text>}

        </View>
    );
}