import { Text, TextInput, View } from "react-native";
import { AutocompleteDropdown } from "react-native-autocomplete-dropdown";
import Municipio from "../../../entidades/Municipio";
import React, { useEffect, useState } from "react";
import { MegaLoginScreenProps } from "../LoginScreen";

export default function MegaAbaMunicipioLogin(props: MegaLoginScreenProps) {
    const { usuario, setUsuario } = props;
    const [municipios, setMunicipios] = useState<Municipio[]>([usuario?.municipio as Municipio]);

    useEffect(() => {
        console.log('34');
        try {
            setMunicipios(Municipio.dados());

        } catch (e) {
            console.error(e);
        }

    }, []);

    function setMunicipioComImagem(municipio: Municipio) {
        setUsuario({
            ...usuario,
            municipio: municipio
        });
    }

    return (
        <View className="px-8 w-full" {...props}>
            <Text className="text-sm text-field-50 font-nunito-semi-bold">
                Prefeitura
            </Text>
            <View className="flex-row w-full justify-center	items-center mt-1">
                <View className="flex-1">
                    <AutocompleteDropdown
                        onSelectItem={item => item && setMunicipioComImagem(item as Municipio)}
                        showClear={false}
                        showChevron={false}
                        direction={'down'}
                        clearOnFocus={false}
                        closeOnSubmit={true}
                        initialValue={usuario?.municipio}
                        EmptyResultComponent={<Text>Nenhuma prefeitura encontrado</Text>}
                        textInputProps={{
                            placeholder: 'Escreva o nome da sua cidade!',
                            style: {
                                backgroundColor: '#fff',
                                borderColor: '#dbdbdb',
                                borderWidth: 1,
                                borderRightWidth: 0,
                                fontFamily: 'Nunito_400Regular'
                            }
                        }}
                        dataSet={municipios}/>
                </View>
                <TextInput className="bg-neutral-110 w-[50] h-[42] border-neutral-210 border rounded-r-[3] text-neutral-210 text-center	text-base font-nunito-semi-bold"
                           editable={false}>
                    {usuario?.municipio ? usuario.municipio.ufDoCliente : 'UF'}
                </TextInput>
            </View>
        </View>
    );
}