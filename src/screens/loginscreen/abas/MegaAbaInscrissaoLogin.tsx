import { Text, TextInput, View } from "react-native";
import React, { useRef, useState } from "react";
import { RadioButton } from "react-native-paper";
import TipoDeInscricaoEnum from "../../../enums/TipoDeInscricaoEnum";
import { MegaLoginScreenProps } from "../LoginScreen";

export default function MegaAbaInscrissaoLogin(props: MegaLoginScreenProps) {
    const { usuario, setUsuario } = props;
    const [tipoDeInscricaoLocal, setTipoDeInscricaoLocal] = useState<string>('imobiliaria');
    const setInscricao = function(inscricao: string): void {
        setUsuario({
            ...usuario,
            inscricao: inscricao
        });
    };

    const alterarInscricao = function(tipoDeInscricao: string): void {
        setTipoDeInscricaoLocal(tipoDeInscricao);
        setUsuario({
            ...usuario,
            inscricao: undefined,
            tipoDeInscricao: tipoDeInscricao as TipoDeInscricaoEnum
        });
    };

    return (
        <View className="px-8 w-full" {...props}>
            <Text className="text-sm text-field-50 font-nunito-semi-bold">
                Tipo de Inscrição
            </Text>
            <View>
                <RadioButton.Group onValueChange={alterarInscricao} value={tipoDeInscricaoLocal}>
                    <View className="flex-row">
                        <RadioButton.Item style={{ paddingHorizontal: 0 }} labelStyle={{ fontFamily: 'Nunito_400Regular', fontSize: 14 }} label="Imobiliária" value="imobiliaria" position="leading"/>
                        <RadioButton.Item style={{ paddingHorizontal: 0 }} labelStyle={{ fontFamily: 'Nunito_400Regular', fontSize: 14 }} label="Municipal" value="municipal" position="leading"/>
                        <RadioButton.Item style={{ paddingHorizontal: 0 }} labelStyle={{ fontFamily: 'Nunito_400Regular', fontSize: 14 }} label="Não tenho" value="naoTenho" position="leading"/>
                    </View>

                    {tipoDeInscricaoLocal === TipoDeInscricaoEnum.IMOBILIARIA && <MegaTextInputInscricaoImobiliaria inscricao={usuario?.inscricao} setInscricao={setInscricao}/>}
                    {tipoDeInscricaoLocal === TipoDeInscricaoEnum.MUNICIPAL && <MegaTextInputInscricaoMunicipal setInscricao={setInscricao}/>}
                    {tipoDeInscricaoLocal === TipoDeInscricaoEnum.NAO_TENHO && <MegaTextInputNaoTenho/>}

                </RadioButton.Group>
            </View>
        </View>
    );
}

interface MegaTextInputInscricaoImobiliariaProps {

    readonly inscricao?: string;

    readonly setInscricao: (inscricao: string) => void;
}

export function MegaTextInputInscricaoImobiliaria({ inscricao, setInscricao }: MegaTextInputInscricaoImobiliariaProps) {
    const inputRefs = Array.from({ length: 5 }, () => useRef<TextInput>(null));

    return (
        <View className="flex-row">
            <MegaTextInputInscricaoImobiliariaAuxiliar opcao={1} referencias={inputRefs} inscricao={inscricao} setInscricao={setInscricao}/>
            <MegaTextInputInscricaoImobiliariaAuxiliar opcao={2} referencias={inputRefs} inscricao={inscricao} setInscricao={setInscricao}/>
            <MegaTextInputInscricaoImobiliariaAuxiliar opcao={3} referencias={inputRefs} inscricao={inscricao} setInscricao={setInscricao}/>
            <MegaTextInputInscricaoImobiliariaAuxiliar opcao={4} referencias={inputRefs} inscricao={inscricao} setInscricao={setInscricao}/>
            <MegaTextInputInscricaoImobiliariaAuxiliar opcao={5} referencias={inputRefs} inscricao={inscricao} setInscricao={setInscricao}/>
        </View>
    );
}

interface MegaTextInputInscricaoImobiliariaAuxiliarProps {
    readonly opcao: number;

    readonly referencias: React.RefObject<TextInput>[];

    readonly inscricao?: string;

    readonly setInscricao: (inscricao: string) => void;

}

export function MegaTextInputInscricaoImobiliariaAuxiliar({ opcao, referencias, inscricao, setInscricao }: MegaTextInputInscricaoImobiliariaAuxiliarProps) {
    const [valor, setValor] = useState<string>('');
    const tamanho = opcao === 1 || opcao === 2 ? 3 : 4;
    let arredondamento: string = '';
    if (opcao === 1) {
        arredondamento = 'rounded-l';
    } else if (opcao === 5) {
        arredondamento = 'rounded-r';
    }

    const setInscricaoImobiliaria = function(texto: string) {
        setInscricao((inscricao ?? '....').split('.')
                                          .map((item, index) => index === opcao - 1 ? texto : item)
                                          .join('.'));

    };

    return (
        <View className="flex-1 w-full h-10">
            <TextInput className={`border border-[#dbdbdb] font-nunito-regular px-3.5 h-10 text-base ${arredondamento}`}
                       keyboardType="numeric"
                       ref={referencias[opcao - 1]}
                       value={valor}
                       blurOnSubmit={opcao === 5}
                       placeholder={''.padStart(tamanho, '0')}
                       onSubmitEditing={() => referencias[opcao]?.current?.focus()}
                       onChangeText={text => {
                           text = text.replace(/[^0-9]/g, '');
                           text = text.padStart(tamanho, '0');
                           if (text.length > tamanho && text.startsWith('0')) {
                               text = text.slice(1, tamanho + 1);
                           }
                           if (text.length === tamanho) {
                               setValor(text);
                               setInscricaoImobiliaria(text);
                           }

                       }}/>
        </View>
    );
}

interface MegaTextInputInscricaoMunicipalProps {
    readonly setInscricao: (inscricao: string) => void;

}

export function MegaTextInputInscricaoMunicipal({ setInscricao }: MegaTextInputInscricaoMunicipalProps) {
    return (
        <TextInput className="border border-[#dbdbdb] font-nunito-regular px-3.5 h-10 text-base rounded"
                   keyboardType={'numeric'}
                   placeholder="Escreva somente números"
                   maxLength={12}
                   onChangeText={setInscricao}/>
    );
}

export function MegaTextInputNaoTenho() {
    return (
        <TextInput className="border border-[#dbdbdb] font-nunito-regular px-3.5 h-10 text-base rounded"
                   keyboardType={'numeric'}
                   placeholder="Consultar Outras Guias"
                   editable={false}
                   maxLength={12}/>
    );
}