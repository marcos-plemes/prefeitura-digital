import axios from "axios";
import Usuario from "../../entidades/Usuario";
import Municipio from "../../entidades/Municipio";
import Contribuinte from "../../entidades/Contribuinte";

export default class LoginService {

    static async buscarMunicipios(): Promise<Municipio[]> {
        const { data } = await axios.get('https://dashboard.cloudmega.com.br/api/cliente-produto/clientes-megaadmweb');
        return data.filter((item: any) => item.codigoDoTipoDoOrgao === 1)
                   .map((item: any) => new Municipio(item));
    }

    static async buscarCodigoDoOrgao(linkDeAcesso: string): Promise<number> {
        const { data: { codigoDoOrgao } } = await axios.get(`${linkDeAcesso}portal-de-servicos/api/configuracao/obter`);
        return codigoDoOrgao;
    }

    static async buscarLogo(linkDeAcesso: string, codigoDoOrgao: number): Promise<string> {
        const config = {
            headers: {
                'Mega-Orgao': codigoDoOrgao
            }
        };
        const { data: { logo } } = await axios.get(`${linkDeAcesso}portal-de-servicos/api/configuracao/imagem-orgao`, config);
        return logo;
    }

    static async buscarContribuinte(usuario: Usuario): Promise<Contribuinte | null> {
        const config = {
            headers: {
                "Authorization":
                    "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.WUxN3kriS_cf-DqdfTxnPk_bE6Hv9yR5_2tt-A6kog0",
                "Content-Type": "application/json",
                "Mega-Orgao": usuario.codigoDoOrgao,
                "Codigo-Funcionalidade": 4
            }
        };

        const { data } = await axios.post(
            `${usuario?.municipio?.linkDeAcesso}portal-de-servicos/api/cidadao/consultar-certidao-negativa-debitos/pesquisar-contribuinte-inscricao-cadastral`,
            { cpfCnpj: usuario.cpfCnpj },
            config);

        return data.length ? new Contribuinte(data) : null;
    }

}