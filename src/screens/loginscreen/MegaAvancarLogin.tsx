import { Alert, Text, TouchableOpacity, View } from "react-native";
import { Button } from "react-native-paper";
import React from "react";
import Usuario from "../../entidades/Usuario";
import AbaLoginEnum from "../../enums/AbaLoginEnum";
import { cnpj, cpf } from "cpf-cnpj-validator";
import LoginService from "./LoginService";
import TipoDeInscricaoEnum from "../../enums/TipoDeInscricaoEnum";
import { MegaLoginScreenProps } from "./LoginScreen";

interface MegaAvancarLoginProps extends MegaLoginScreenProps {
    readonly aba: number;

    readonly setAba: (aba: number) => void;
}

export default function MegaAvancarLogin({ aba, setAba, usuario, setUsuario }: MegaAvancarLoginProps) {

    const avancarInscricaoHabilitado = function() {
        let isAvancar: boolean;

        switch (usuario?.tipoDeInscricao) {
            case TipoDeInscricaoEnum.IMOBILIARIA:
                isAvancar = usuario?.inscricao?.length === 22;
                break;
            case TipoDeInscricaoEnum.MUNICIPAL:
                isAvancar = (usuario?.inscricao?.length ?? 0) >= 4;
                break;
            case TipoDeInscricaoEnum.NAO_TENHO:
                isAvancar = true;
                break;
            default:
                isAvancar = usuario?.inscricao?.length === 22;
                break;
        }

        return isAvancar;
    };

    const avancarHabilitado = function() {
        let isAvancar: boolean = false;

        switch (aba) {
            case AbaLoginEnum.MUNICIPIO:
                isAvancar = usuario?.municipio !== undefined;
                break;
            case AbaLoginEnum.CONTRIBUINTE:
                isAvancar = cpf.isValid(usuario?.cpfCnpj as string) || cnpj.isValid(usuario?.cpfCnpj as string);
                break;
            case AbaLoginEnum.INSCRICAO:
                isAvancar = avancarInscricaoHabilitado();
                break;
            default:
                break;
        }

        return isAvancar;
    };

    const validarAvancoContribuinte = async function(): Promise<boolean> {
        const contribuinte = await LoginService.buscarContribuinte(usuario as Usuario);

        if (contribuinte) {
            setUsuario({
                ...usuario,
                contribuinte: contribuinte
            });
        } else {
            Alert.alert(
                'ATENÇÃO',
                'O CPF/CNPJ informado não foi encontrado no Município!',
                [{ text: 'OK' }]);
        }

        return contribuinte !== null;

    };

    const validarAvancoInscricao = async function() {
        let isAvancar: boolean;

        switch (usuario?.tipoDeInscricao) {
            case TipoDeInscricaoEnum.IMOBILIARIA:
                isAvancar = usuario.contribuinte?.inscricaoImobiliaria?.includes(usuario.inscricao as string) ?? false;
                break;
            case TipoDeInscricaoEnum.MUNICIPAL:
                isAvancar = usuario.contribuinte?.inscricaoMunicipal?.includes(usuario.inscricao ? Number(usuario.inscricao) : -1) ?? false;
                break;
            default:
                isAvancar = true;
                break;
        }

        if (!isAvancar) {
            Alert.alert(
                'ATENÇÃO',
                'Inscrição incorreta entrar com outra inscrição Válida!',
                [{ text: 'OK' }]);
        }

        return isAvancar;

    };

    const avancarLogin = function() {

        switch (aba) {
            case AbaLoginEnum.MUNICIPIO:
                setAba(AbaLoginEnum.CONTRIBUINTE);
                break;
            case AbaLoginEnum.CONTRIBUINTE:
                validarAvancoContribuinte().then(isAvancar => isAvancar && setAba(AbaLoginEnum.INSCRICAO));
                break;
            case AbaLoginEnum.INSCRICAO:
                validarAvancoInscricao();
                break;
            default:
                break;

        }

    };

    return (
        <>
            <View className="px-8 mt-4 w-full">
                <Button className={`h-[45] rounded-[3px] border border-b-[3px] ${avancarHabilitado() ? 'bg-[#d7ebfa]' : 'bg-[#efefef]'}`}
                        onPress={avancarLogin}
                        disabled={!avancarHabilitado()}
                        style={{ borderColor: avancarHabilitado() ? '#afcee6' : '#d3d3d3' }}>
                    <Text className={`text-xl font-nunito-bold mt-[3px] ${avancarHabilitado() ? 'text-[#486389]' : 'text-[#d3d3d3]'}`}>
                        AVANÇAR
                    </Text>
                </Button>
            </View>
            <View className="mt-5 flex-row">
                <MegaAbaButton aba={AbaLoginEnum.MUNICIPIO} abaAtual={aba} setAbaAtual={setAba}/>
                <View className="mt-[13] w-[30] h-px bg-[#e5e5e5]"/>
                <MegaAbaButton aba={AbaLoginEnum.CONTRIBUINTE} abaAtual={aba} setAbaAtual={setAba}/>
                <View className="mt-[13] w-[30] h-px bg-[#e5e5e5]"/>
                <MegaAbaButton aba={AbaLoginEnum.INSCRICAO} abaAtual={aba} setAbaAtual={setAba}/>
            </View>
        </>
    );
}

interface MegaAbaButtonProps {
    readonly aba: number;

    readonly abaAtual: number;

    setAbaAtual: (aba: number) => void;
}

export function MegaAbaButton({ aba, abaAtual, setAbaAtual }: Readonly<MegaAbaButtonProps>) {
    return (
        <TouchableOpacity className={`rounded-full px-2.5 py-[5px] ${aba === abaAtual ? 'bg-[#afcee6]' : 'bg-neutral-110'}`} disabled={aba >= abaAtual} onPress={() => setAbaAtual(aba)}>
            <Text className={`text-xs ${aba === abaAtual ? 'text-white' : 'text-aba-button-1'}`} style={{}}>
                {aba}
            </Text>
        </TouchableOpacity>
    );
}