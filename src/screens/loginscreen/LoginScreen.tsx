import { SafeAreaView, ViewProps } from "react-native";
import ClipPathHeader from "../../clippath/ClipPathHeader";
import MegaLogoLogin from "./MegaLogoLogin";
import { AutocompleteDropdownContextProvider } from "react-native-autocomplete-dropdown";
import MegaAbaMunicipioLogin from "./abas/MegaAbaMunicipioLogin";
import MegaAbaContribuinteLogin from "./abas/MegaAbaContribuinteLogin";
import MegaAbaInscrissaoLogin from "./abas/MegaAbaInscrissaoLogin";
import MegaAvancarLogin from "./MegaAvancarLogin";
import MegaFooter from "../../components/MegaFooter";
import React, { useState } from "react";
import Usuario from "../../entidades/Usuario";
import AbaLoginEnum from "../../enums/AbaLoginEnum";

export interface MegaLoginScreenProps extends ViewProps {
    readonly usuario?: Usuario;

    readonly setUsuario: (usuario: Usuario) => void;

}

export default function() {
    const [aba, setAba] = useState(1);
    const [usuario, setUsuario] = useState<Usuario>();

    const propriedades: MegaLoginScreenProps = {
        usuario: usuario,
        setUsuario: setUsuario
    };

    return (
        <SafeAreaView className="flex-1 items-center justify-center bg-white">
            <ClipPathHeader/>
            <MegaLogoLogin usuario={usuario} setUsuario={setUsuario}/>
            <AutocompleteDropdownContextProvider>
                <MegaAbaMunicipioLogin {...propriedades} style={{ display: aba === AbaLoginEnum.MUNICIPIO ? 'flex' : 'none' }}/>
                <MegaAbaContribuinteLogin {...propriedades} style={{ display: aba === AbaLoginEnum.CONTRIBUINTE ? 'flex' : 'none' }}/>
                <MegaAbaInscrissaoLogin {...propriedades} style={{ display: aba === AbaLoginEnum.INSCRICAO ? 'flex' : 'none' }}/>
                <MegaAvancarLogin aba={aba} setAba={setAba} {...propriedades}/>

            </AutocompleteDropdownContextProvider>

            <MegaFooter/>
        </SafeAreaView>
    );
}