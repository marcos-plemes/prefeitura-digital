import { Keyboard, Platform, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import MarcaMegasoft from "../../assets/icons/marca-megasoft-dark.svg";

export default function MegaFooter() {
    const [tecladoAberto, setTecladoAberto] = useState(false);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow',
            () => {
                setTecladoAberto(true);
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide',
            () => {
                setTecladoAberto(false);
            }
        );

        // Cleanup function
        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    return (
        <>
            {!tecladoAberto && <View className="flex-row h-12 bg-white w-full absolute bottom-0 items-center justify-center">
                <View>
                    <Text className="text-right pt-1 text-[11px]">
                        Copyright © 2020
                    </Text>
                    <Text className="text-right text-[11px]">
                        Produto desenvolvido por Megasoft
                    </Text>
                </View>

                <MarcaMegasoft height="30" width="120" viewBox="15 -10 100 60"/>
            </View>}
        </>
    );
};