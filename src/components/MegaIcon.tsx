import { createIconSetFromIcoMoon } from "@expo/vector-icons";

export default createIconSetFromIcoMoon(
    require("../../assets/fonts/mega-icon/selection.json"),
    "IcoMoon",
    "mega-icon.ttf"
);
