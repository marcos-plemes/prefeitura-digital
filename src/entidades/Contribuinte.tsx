export default class Contribuinte {
    nome: string;

    cpfCnpj: string;

    inscricaoMunicipal?: number[];

    inscricaoImobiliaria?: string[];

    constructor(dados: any) {
        this.nome = dados[0].nome;
        this.cpfCnpj = dados[0].cpfCnpj;
        this.inscricaoMunicipal = [...new Set(dados.map((item: any) => item.inscricaoMunicipal))] as number[];
        this.inscricaoImobiliaria = [...new Set(dados.map((item: any) => item.inscricaoImobiliaria))] as string[];
    }
}