import { TAutocompleteDropdownItem } from "react-native-autocomplete-dropdown";

export default class Municipio implements TAutocompleteDropdownItem {

    id: string;

    title: string;

    linkDeAcesso: string;

    ufDoCliente: string;

    constructor(data: any) {
        this.id = data.codigoDoCliente.toString();
        this.title = data.cidadeDoCliente;
        this.linkDeAcesso = data.linkAcesso;
        this.ufDoCliente = data.ufDoCliente;
    }

    static dados() {
        return [
            {
                "codigoDoCliente": 2,
                "cidadeDoCliente": "Bela Vista de Goiás",
                "ufDoCliente": "GO",
                "nomeDoCliente": "CÂMARA MUN. DE BELA VISTA DE GOIÁS",
                "codigoDoTipoDoOrgao": 2,
                "tipoDoOrgao": "Poder Legislativo",
                "linkAcesso": "http://192.168.1.13:8082/"
            },
            {
                "codigoDoCliente": 8,
                "cidadeDoCliente": "Morrinhos",
                "ufDoCliente": "GO",
                "nomeDoCliente": "CÂMARA MUNICIPAL DE MORRINHOS",
                "codigoDoTipoDoOrgao": 2,
                "tipoDoOrgao": "Poder Legislativo",
                "linkAcesso": "https://camaramorrinhos.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 9,
                "cidadeDoCliente": "Novo Gama",
                "ufDoCliente": "GO",
                "nomeDoCliente": "CÂMARA MUNICIPAL DE NOVO GAMA",
                "codigoDoTipoDoOrgao": 2,
                "tipoDoOrgao": "Poder Legislativo",
                "linkAcesso": "https://camaranovogama.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 11,
                "cidadeDoCliente": "Pires do Rio",
                "ufDoCliente": "GO",
                "nomeDoCliente": "CÂMARA MUN. DE PIRES DO RIO",
                "codigoDoTipoDoOrgao": 2,
                "tipoDoOrgao": "Poder Legislativo",
                "linkAcesso": "https://camarapiresdorio.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 49,
                "cidadeDoCliente": "Alto Horizonte",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE ALTO HORIZONTE",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://altohorizonte.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 50,
                "cidadeDoCliente": "Amaralina",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUN. DE AMARALINA",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://amaralina.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 53,
                "cidadeDoCliente": "Baliza",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUN. DE BALIZA",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://baliza.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 54,
                "cidadeDoCliente": "Barro Alto",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUN. DE BARRO ALTO",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://barroalto.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 55,
                "cidadeDoCliente": "Bonópolis",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE BONÓPOLIS",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://bonopolis.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 59,
                "cidadeDoCliente": "Campinorte",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE CAMPINORTE",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://campinorte.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 61,
                "cidadeDoCliente": "Cavalcante",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE CAVALCANTE",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://cavalcante.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 62,
                "cidadeDoCliente": "Corumbaíba",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUN. DE CORUMBAÍBA",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://corumbaiba.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 64,
                "cidadeDoCliente": "Diorama",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE DIORAMA",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://diorama.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 65,
                "cidadeDoCliente": "Doverlândia",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE DOVERLÂNDIA",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://doverlandia.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 73,
                "cidadeDoCliente": "Mara Rosa",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUN. DE MARA ROSA",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://mararosa.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 76,
                "cidadeDoCliente": "Montividiu do Norte",
                "ufDoCliente": "GO",
                "nomeDoCliente": "PREFEITURA MUN. DE MONTIVIDIU DO NORTE",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://montividiudonorte.megaadmweb.com.br/"
            },
            {
                "codigoDoCliente": 77,
                "cidadeDoCliente": "Nerópolis",
                "ufDoCliente": "TO",
                "nomeDoCliente": "PREFEITURA MUNICIPAL DE NERÓPOLIS",
                "codigoDoTipoDoOrgao": 1,
                "tipoDoOrgao": "Poder Executivo",
                "linkAcesso": "https://neropolis.megaadmweb.com.br/"
            }].map((item: any) => new Municipio(item));
    }

}

