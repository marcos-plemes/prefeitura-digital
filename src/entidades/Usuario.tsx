import Municipio from "./Municipio";
import Contribuinte from "./Contribuinte";
import TipoDeInscricaoEnum from "../enums/TipoDeInscricaoEnum";

export default interface Usuario {
    municipio?: Municipio;

    cpfCnpj?: string;

    codigoDoOrgao?: number;

    contribuinte?: Contribuinte;

    inscricao?: string;

    tipoDeInscricao?: TipoDeInscricaoEnum;
}